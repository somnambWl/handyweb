#######
Jupyter
#######

Spusteni notebooku
==================

Jupyter notebook spustite prikazem (nejprve se presunte do slozky, kde chcete notebook spustit)::

    jupyter notebook

Pokud chcete spustit pouze konkretni notebook::

    jupyter notebook NazevNotebooku

