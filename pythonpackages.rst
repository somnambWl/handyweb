###############
Pythoní balíčky
###############

Struktura balíčku
================

Struktura balíčku by mohla vypadat nějak takto:

::

    |- NazevProjektu
    |   |- setup.py
    |   |- README.md
    |   |- HlavniNazev
    |   |   |- __init__.py
    |   |   |- VedlejsiNazev
    |   |   |   |- __init__.py
    |   |   |   |- skripty.py
    |   |- prikazy.py

*NazevProjektu* je prostě pouze název projektu, hlavní složky, ve které je uložen a pod tímto jménem jej budete mít i na bitbucketu.

***setup.py*** je soubor, ve kterém lze nalézt nejdůležitější informace o balíčku a slouží k instalaci.

***README.md*** se zobrazuje na první stránce projektu na bitbucketu.

*HlavniNazev* je název Vašeho balíčku.
V této složce se vytváří další složky, které teprve obsahují skripty.

První ***__init__.py*** obsahuje pouze::

    __import__('pkg_resources').declare_namespace(__name__)

*VedlejsiNazev* je název nějaké části celého balíku.
Těchto složek tu může být mnoho.

Druhý ***__init__.py*** by měl obsahovat importy všech funkcí, které jsou potřeba v *prikazy.py*.

*skripty.py* by měly obsahovat obecné funkce, které se volají z *prikazy.py*.
Těchto souborů tu znovu může být libovolně mnoho.

*prikazy.py* by měly obsahovat všechny spustitelné příkazy, které balíček má.

Příklad struktury
-----------------

setup.py
========

::

    from setuptools import setup, find_packages
    from setuptools.command.test import test as TestCommand

    version = "0.1.0"

    setup(name="NazevProjektu",
          version=version,
          scripts=["probe_utils.py"],
          description="Utilities for probe simulations",
          long_description="""\
          """,
          classifiers=[],  # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
          keywords="",
          author="Jakub Wagner",
          author_email="somnambWl@gmail.com",
          url="",
          license="GPLv3",
          packages=find_packages(exclude=["ez_setup", "examples", "tests"]),
          include_package_data=True,
          zip_safe=True,
          install_requires=[
          # -*- Extra requirements: -*-
          "cleo",
          "h5py",
          ],
          entry_points="""
          # -*- Entry points: -*-
          """,
          tests_require=["pytest"],
          namespace_packages=["probe"],
          cmdclass={"test": PyTest}
          )

