###
Vim
###

Instalace balíčků
=================

Jelikož vim má složky pro skripty, nápovědy apod., tak je pro uživatele trochu nepřehledné něco hledat a vědět, kde se co nachází.
Když si nainstalujete skript Pathogen, tak každý skript, balíček apod. bude mít svou vlastní složku ve *~/.vim/bundle/*, což je trochu přehlednější.

Pathogen
--------

Nejprve začněte tím, že ve složce *~/.vim/* vytvoříte složky::

    mkdir autoload
    mkdir bundle

Ve složce autoload použijte příkaz::

    curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

Do souboru *~/.vimrc*, což je konfigurační soubor vimu připište::

    set nocompatible
    execute pathogen#infect()
    call pathogen#helptags()

Vimrc
=====

Užitečné příkazy
================

Nahrazení výrazu
----------------

Pro nahrazení výrazu na několika řádcích současně použijte::

    :1,5s/slovo/nahrazene/gc

Takto Vám vim označí všechny výrazy na daných řádcích (1 až 5) a postupně se Vás bude ptát, zda ho má nahradit.

Kopírování velkého množství věcí
--------------------------------

Příkaz *:set wrap* dlouhé řádky zalomí tak, aby se všechno vlezlo na Vaši obrazovku::

    :set wrap
    
Zvýšení čísla
-------------

Aniž by jste vstupovali do insert módu, můžete zvýšit číslo, na kterém máte kurzor, stiskem *CTRL+A*.
Pokud předtím zmáčknete nějaké číslo, číslo na kurzoru se zvětší o Vámi stisknuté číslo, ne o jedničku.


Začátek
=======

Nejprve začněte tím, že do terminálu napíšete *vimtutor* a projdete si tento tutoriál.


