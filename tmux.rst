####
Tmux
####

Konfigurační soubor
===================

Vytvořte soubor *~/.tmux.conf* a vepište do něj::

    unbind C-b
    set -g prefix C-a
    bind C-a send-prefix

Používání a příkazy
===================

Doporučuji začít tím, že napíšete::

    tmux ls

Tak zjistíte, zda už máte vytvořený nějaký tmux, popřípadě kolik, jak se jmenují a kolik mají otevřených oken.
K nějakému z těchto terminálu se připojíte pomocí::

    tmux attach -t NazevTmuxu

Pokud žádný tmux spuštěn napište prostě *tmux*.

Okno přejmenujete::

    Ctrl+a ,

Celé tmuxovské sezení přejmenujete::

    Ctrl+a $

Odpojíte se::

    Ctrl+a d

