###
Git
###

Zacatky
=======

Po vytvoreni slozky, ve ktere je Vas projekt napiste::

    git init

Dalsim dulezitym prikazem je ::

    git status
    
, ze ktereho se dozvite, zda pribyli nejake nove soubory, 
ktere jeste nemate verzovane pomoci gitu.

Po vytvoreni noveho souboru jej muzete zaverzovat pomoci (nakonec pridat jmeno souboru)::

    git add 

Dale ale jeste musite pouzit (nakonec opet pridat jmeno souboru)::

    git commit -m

, kde *-m* slouzi k napsani zpravy mezi uvozovky. 
Durazne doporucuji takto pouzivat a vzdy napsat alespon kratkou zpravu, co se zmenilo.

Zalozeni repositare
===================

S predchozimi prikazy muzete pracovat sami na svem pocitaci, 
ale pokud chcete mit vse ulozene na bitbucketu, 
musite jeste slozku nahrat na bitbucket::

    git remote add origin git@bitbucket.org:user/nameofrepository.git

Prikazem::

    git push

Vse, co mate commitle poslete na bitbucket.

Mene pouzivam
=============

Prikaz::

    git log

Vam dava informaci o ruznych verzich souboru, ktere jste commitli.

Cizi ucet
=========

Kliknutim na *Fork* v cizim projektu si ho zkopirujete k sobe.

Prikazem::

    git clone git@bitbucket:user/directory.git

si stahnete cely projekt (at svuj nebo cizi, pokud ho mate na uctu).

Neco se pokazilo!
=================

Kdyz se neco pokazi a vy chcete vse vratit do posledniho commitu, pouzijte::

    git reflog

Ten Vam vypise posledni commity, vyberte si nejaky (treba ten posledni, pokud se do nej chcete vratit) a dale pouzijte::

    git reset --hard NumberOfCommit

