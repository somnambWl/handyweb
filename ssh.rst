####
ssh
####

Abyste se mohli připojit na vzdálený počítač bez neustálého zadávání hesla, vygenerujte nejprve na svém počítači klíč::

    ssh-keygen

Vše odklikejte a poté tento klíč zkopírujte na vzdálený počítač::

    ssh-copy-id user@hostname.example.com

