.. HandyWeb documentation master file, created by
   sphinx-quickstart on Sun May 31 22:03:27 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Vítejte
=======

Rozhodl jsem se na tento web co nejpřehledněji zapisovat věci,
které se naučím, tak doufám, že z toho budete mít něco i Vy. 

Tato stránka ještě není úplně dodělaná,
některé kapitoly obsahují pouze nadpisy, ale brzy přibude i obsah.

Obsah:

.. toctree::
   :maxdepth: 2

   python.rst
   gnuplot.rst
   latex.rst
   vim.rst 
   tmux.rst
   terminal.rst
   git.rst
   pythonpackages.rst
   markdown.rst
   ssh.rst
   jupyter.rst
