Markdown
########

Tabulky
=======

Tabulky se v markdownu dělají pomocí "|". 
Tento znak od sebe odděluje sloupce.

Na první řádek se píší nadpisy, druhý řádek musí být vyplněn napřeskáčku spojníky a čarami "-|-|-|-", přičemž se začíná spojníkem 
