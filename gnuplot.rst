####
Gnuplot
####

Tvorba obrázků
==============

Nejprve nastavte v gnuplotu správný terminál a velikost obrázku::

    set term pngcairo size 1280,768

Dále přidejte k vykonávaným příkazům::

    set output "NázevObrázku.png";

Příkazy můžete zadávat stejně jako předtím a obrázky se zadaným názvem se Vám budou ukládat do složky, kde jste gnuplot otevřeli.

Vykreslování obrázků
====================

Pokud máte obrázek uložený a chcete ho vykreslit pomocí gnuplotu, uděláte tak příkazem::

    plot ".image.dat" with image

Funkce a proměnné
=================

V gnuplotu můžete naprosto normálně vytvářet proměnné::

    NázevKonstanty = HodnotaKonstanty

Dále můžete definovat funkce a používat v nich již definované proměnné::

    f(x) = a*x + b

Fitování dat
============

Pokud máte data, která chcete nafitovat pomocí nějaké Vámi určené funkce, nejprve funkci nadefinujte a poté napište::

    fit f(x) "./Data.txt"

Takto fitujete data uložená v souboru "Data.txt" pomocí funkce *f(x)* kterou jste předtím definovali.

Pokud víte, že fitujete, například, lineární závislost a víte hodnotu jednoho koeficientu, můžete ho nastavit jako proměnnou a fitovat jen pomocí jedné proměnné::

    fit f(x) "./Data.txt" via b

Takto se mění pouze parametr *b*, přičemž parametr *a* jste již zadali.

Palety
======

Pokud se Vám nelíbí základní nastavení barev v gnuplotu, dá se lehce změnit na nějakou jinou přednastavenou nebo na kombinaci tří funkcí.

Pomocí funkcí ji změníme takto::

    set palette rgbformulae __,__,__

kde prázdná místa jsou čísla funkcí (je jich něco přes třicet).

Některé zajímavé palety:

- gray = černobílá

- 21,22,23 = historická

