#####
Latex
#####

Vkládání obrázků
================

Na vkládání obrázků, které centruji používám::

    \begin{center}
    \includegraphics[scale=0.5]{NazevObrazkuVeSlozce.png}
    \captionof{figure}{NazevKterySeZobrazi}
    \end{center}

Tabulky
=======

Na tabulky používám::

    \begin{table}[h]
    \caption{NazevTabulky}
    \begin{adjustwidth}{-.5in}{-.5in}  
    \begin{center}
    \begin{tabular}{}

    \end{tabular}
    \end{center}
    \end{adjustwidth}
    \end{table}

Tabulka je pak centrovaná, má nadpis, který si můžete zvolit a navíc, pokud je široká, tak se roztahuje od středu a vleze se na celou stránku A4, přičemž ignoruje okraje.

Pokud stránku dělíte na části, nezapomeňte, že prostředí *minipage* se vkládá až za prostředí *table*.

Tečky za čísly kapitol
======================

Tečky za čísly kapitol a podkapitol přidáte příkazy::

    \renewcommand{\thesection}{\arabic{section}.}
    \renewcommand{\thesubsection}{\thesection\arabic{subsection}.}

Odkazování na rovnice v textu
=============================

Pokud používáte pro tvorbu rovnic *equation* a ne dolary, můžete dovnitř vložit *label* a rovnici tak pojmenovat a později na ni v textu odkazovat.

Tedy rovnice vypadají nějak takto::

    \begin{equation}
    \label{NazevRovnice}

    \end{equation} 
    
A v textu na ně odkážete pomocí *eqref*::

    \eqref{NazevRovnice}

Šablony v texmakeru pro vytváření nových souborů
================================================

V texmakeru najděte v menu ``Uživatel ---> Uživatelské značky ---> Upravit uživatelské značky``, pak si nějakou vyberte, pojmenujte a zkopírujte tam text, který chcete používat jako šablonu.

Poté, když vytvoříte nový soubor tak zmáčknete například jen *SHIFT+F1* a můžete psát do šablony. 

Doporučuji používat tečky místo nadpisů, aby jste mohli rychle doplňovat nadpisy pomocí tabulátoru









