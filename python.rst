Python
######

Na kopírování
-------------

::

    #!/usr/bin/env python


Zkratky
-------

Budu používat takto importované balíčky::

    import numpy as np
    import matplotlib.pyplot as plt

Zpracování dat
==============

Asi nejdůležitější příkaz pro zpracovávání dat je::

    np.loadtxt("CestaKSouboru", skiprows=PocetPreskocenychRadku, usecols=(CisloSloupce,), delmiter="Rozdelovac")

První sloupec je samozřejmě nultý.



Grafy
=====

Základní příkazy jsou::

    plt.plot(x, y, label="Popis")
    plt.xlabel("NazevX-oveOsy")
    plt.ylabel("NazevY-oveOsy")
    plt.legend(loc="KdeUmistit")
    plt.show()

Jako místo, kde se umístí legenda se nejčastěji používají:

* upper right
* upper left
* lower right
* lower left
